<?php

use \EasySwoole\EasySwoole\Command\CommandContainer;
use \App\Cron\Command\Test;


CommandContainer::getInstance()->set(new Test()); //注入自定义命令行
CommandContainer::getInstance()->set(new \App\Cron\Command\Tianyan()); //注入自定义命令行
CommandContainer::getInstance()->set(new \App\Cron\Command\FastCacheQueue()); //消费 FastCacheQueue 队列
CommandContainer::getInstance()->set(new \App\Cron\Command\TcpClient()); //tcp 客服端
CommandContainer::getInstance()->set(new \App\Cron\Command\Pay()); //支付接口测试
CommandContainer::getInstance()->set(new \App\Cron\Command\Demo());




