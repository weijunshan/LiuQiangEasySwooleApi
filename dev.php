<?php
return [
    'SERVER_NAME' => "EasySwoole",
    'MAIN_SERVER' => [
        'LISTEN_ADDRESS' => '0.0.0.0',
        'PORT' => 9501,
        'SERVER_TYPE' => EASYSWOOLE_WEB_SOCKET_SERVER, //可选为 EASYSWOOLE_SERVER  EASYSWOOLE_WEB_SERVER EASYSWOOLE_WEB_SOCKET_SERVER,EASYSWOOLE_REDIS_SERVER
        'SOCK_TYPE' => SWOOLE_TCP,
        'RUN_MODEL' => SWOOLE_PROCESS,
        'SETTING' => [
            'worker_num' => 8,
            'reload_async' => true,
            'max_wait_time' => 3
        ],
        'TASK' => [
            'workerNum' => 4,
            'maxRunningNum' => 128,
            'timeout' => 15
        ]
    ],
    'TEMP_DIR' => null,
    'LOG_DIR' => null,
    'MYSQL' => [
        //数据库配置
        'host' => '127.0.0.1',//数据库连接ip
        'user' => 'root',//数据库用户名
        'password' => 'liuqiang',//数据库密码
        'database' => 'easy_api',//数据库
        'port' => '3306',//端口
        'timeout' => '10',//超时时间
        'connect_timeout' => '5',//连接超时时间
        'charset' => 'utf8',//字符编码
        'strict_type' => false, //开启严格模式，返回的字段将自动转为数字类型
        'fetch_mode' => false,//开启fetch模式, 可与pdo一样使用fetch/fetchAll逐行或获取全部结果集(4.0版本以上)
        'alias' => '',//子查询别名
        'isSubQuery' => false,//是否为子查询
        'max_reconnect_times ' => '3',//最大重连次数
    ],
    'REDIS' => [
        'host' => '127.0.0.1',
        'port' => '6379',
        'auth' => null,
        'timeout' => 3, //超时时间3秒
        'reconnectTimes' => 3,//重新连接时间
        'db' => null,
        'serialize' => \EasySwoole\Redis\Config\RedisConfig::SERIALIZE_NONE
    ],
    //redis连接池名称
    'REDIS_POOL_NAME' => [
        'is_rand' => false, //是否随机获取连接池  这个true 就在pool_name里面随机获取 否则 就是默认值
        'pool_name' => [],
        'default' => 'redis_pool'
    ],
    'JWT' => [
        'iss' => 'liuqiang', // 发行人
        'exp' => 3600, // 过期时间 默认1小时 1*60*60=7200
        'sub' => 'easyApiLiuQiang', // 主题
        'nbf' => NULL, // 在此之前不可用
        'key' => '9536948BF99C9A4259B3C36FF5BE84A5', // 签名用的key
    ],
    'TCP_DATA' => [ //tcp 数据包处理配置
        'open_length_check' => false,
        'set_data' => [
            'open_length_check' => true,
            'package_max_length' => 1240,
            'package_length_type' => 'N',
            'package_length_offset' => 0,
            'package_body_offset' => 4,
        ]
    ],
    'RABBITMQ' => [  //rabbitmq三方中间 配置文件
        'host' => '127.0.0.1',
        'port' => 5672,
        'user' => 'root',
        'pwd' => 'root',
        'vhost' => '/',
        'pool_name'=>'rabbitmq_pool'
    ]
];
