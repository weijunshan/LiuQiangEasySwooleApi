<?php


namespace App\Cron\QueueProcess;


use App\Cron\Queue\TestQueue;
use EasySwoole\Component\Process\AbstractProcess;
use EasySwoole\Queue\Job;

class TestQueueProcess extends AbstractProcess
{

    protected function run($arg)
    {
        foreach (range(0, 10) as $v) { //开启10个协程 处理
            go(function () {
                TestQueue::getInstance()->consumer()->listen(function (Job $job) {
                    var_dump($job->getJobId());
                    var_dump($job->toArray());
                });
            });
            echo $v, PHP_EOL;
        }
    }
}
