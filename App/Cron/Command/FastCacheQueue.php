<?php


namespace App\Cron\Command;


use EasySwoole\EasySwoole\Command\CommandInterface;
use EasySwoole\FastCache\Cache;
use Swoole\Coroutine;

class FastCacheQueue implements CommandInterface
{

    public function commandName(): string
    {
        return 'fast_cache_queue';
    }

    public function exec(array $args): ?string
    {
//        $wait = new \EasySwoole\Component\WaitGroup();
        foreach (range(0, 50) as $v) {
            go(function () {
                while (true) {
                    $job = Cache::getInstance()->getJob('fast_cache_queue');// Job对象或者null
                    if ($job === null) {
//                        echo \time() . '--没有任务', PHP_EOL;
                        Coroutine::sleep(1);
                    } else {
                        // 执行业务逻辑
                        var_dump($job);
                        // 执行完了要删除或者重发，否则超时会自动重发
                        Cache::getInstance()->deleteJob($job);
                    }
                }
            });
            echo $v,PHP_EOL;
        }
        // 等待协程结束在退出
//        $wait->wait();
        return true;
    }

    public function help(array $args): ?string
    {
        // TODO: Implement help() method.
    }
}
