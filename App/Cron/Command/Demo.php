<?php


namespace App\Cron\Command;


use App\Common\Sftp;
use EasySwoole\EasySwoole\Command\CommandInterface;

class Demo implements CommandInterface
{
    protected $conf;


    public function commandName(): string
    {
        return 'demo';
    }

    public function exec(array $args): ?string
    {
        $str_48 = '110000000000000000000100100000000000000000000011';
        $start = '2020-01-15 00:30:00';
        $this->suanfa($start, $str_48);
        exit();
//        var_dump(decbin(16));
        exit();
//        spl_autoload_register(function ($class_name) {
//            var_dump($class_name);exit();
//        });
        $conf = [
            'host' => '140.205.103.244', //服务器
            'port' => '22', //端口
            'username' => 'yqzl001_dada', //用户名
            'password' => 'dada_123456', //密码
        ];
        $sftp = new Sftp($conf);
        var_dump($sftp);
        exit();

        $data = [
            'head' => [
                'Version' => '2.0.0',
                'Appid' => 2014000014442,
                'Function' => 'tesdt',
                'ReqTime' => date('Y-m-dHms'),
                'ReqTimeZone' => 'Asia/Shanghai',
                'ReqMsgId' => uuid_create(true),
                'Reserve' => 'kv',
                'SignType' => 'RSA',
                'InputCharset' => 'UTF-8'
            ],
            'body' => [
                'userId' => 12333
            ],
            'ds:Signature' => 'sssss'
        ];
        $this->request_xml($data);
        return true;
    }

    public function help(array $args): ?string
    {
        // TODO: Implement help() method.
    }

    protected function request_xml($data)
    {
        $xml = '<?xml version="1.0" encoding="utf-8" ?><document><request id="request"><head>';
//        1.拼接head头部信息
        foreach ($data['head'] as $headKey => $headVal) {
            $xml .= '<' . $headKey . '>' . $headVal . '</' . $headKey . '>';
        }
        $xml .= '</head><body>';
//          2.拼接body信息
        foreach ($data['body'] as $bodyKey => $bodyVal) {
            $xml .= '<' . $bodyKey . '>' . $bodyVal . '</' . $bodyKey . '>';
        }
        $xml .= '</body>';
        $xml .= '</request><Signature>' . $data['ds:Signature'] . '</Signature></document>';
        libxml_disable_entity_loader(true);
        $da = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        var_dump($da);
    }

    /**
     * @param $partnerId int 合作伙伴id
     * @param $order string 平台生成的唯一订单号
     * @return string
     */
    protected function name($partnerId, $order)
    {
        $name = ['CCF', $partnerId, date('Ymd'), $order];
        return implode('_', $name) . '.csv';
    }

    protected function send_sms()
    {
        $data = [
            'partner' => $this->conf['partner_id'],
            'bizNo' => $this->conf['order'],
            'bizName' => 'P_BATCH_TRANSFER',
            'certNo' => $this->conf['certNo'],//操作员身份证号码
            'sign' => ''
        ];
    }

    protected function define()
    {
        $data = [
            'partner' => $this->conf['partner_id'],
            'bizNo' => $this->conf['order'],
            'bizName' => 'P_BATCH_TRANSFER',
            'smsCode' => '',//短信验证码
            'fileName' => $this->name($this->conf['partner_id'], $this->conf['order']),
            'totalCount' => '2',//总条数
            'totalAmount' => '10', //总金额
            'currencyCode' => 'CNY', //币种
            'remark' => '代付工资', //备注
            'companyCardNo' => '8888888791870817', //付款卡号
            'sign' => ''
        ];
    }

    /**
     * 生成签名
     * @param string $signString 待签名字符串
     * @param    [type]     $priKey     私钥
     * @return   string     base64结果值
     */
    protected function getSign($signString, $priKey)
    {
        $privKeyId = openssl_pkey_get_private($priKey);
        $signature = '';
        openssl_sign($signString, $signature, $privKeyId);
        openssl_free_key($privKeyId);
        return urlencode($signature);
    }

    /**
     * 校验签名
     * @param string $pubKey 公钥
     * @param string $sign 签名
     * @param string $toSign 待签名字符串
     * @return   bool
     */
    protected function checkSign($pubKey, $sign, $toSign)
    {
        $publicKeyId = openssl_pkey_get_public($pubKey);
        $result = openssl_verify($toSign, base64_decode($sign), $publicKeyId);
        openssl_free_key($publicKeyId);
        return $result === 1 ? true : false;
    }


    /**
     * 获取待签名字符串
     * @param array $params 参数数组
     * @return   string
     */
    function getSignString($params)
    {
        unset($params['sign']);
        ksort($params);
        $pairs = array();
        foreach ($params as $k => $v) {
            if (!empty($v)) {
                $pairs[] = "$k=$v";
            }
        }
        return implode('&', $pairs);
    }


    public function suanfa($start, $str_48)
    {
        $totalTime = substr_count($str_48, '1') * 30 / 60;
        $startDate = strtotime(date('Y-m-d', strtotime($start)) . ' 00:00:00');
        $startTimeNum = date('H', strtotime($start)) * 2 + ceil(date('i', strtotime($start)) / 30);
        $startTotal = substr_count(substr($str_48, $startTimeNum - 48), '1') * 30 / 60;
        $endTime = time();
        $endDate = strtotime(date('Y-m-d', $endTime) . ' 00:00:00');
        $endTimeNum = date('H', $endTime) * 2 + ceil(date('i', $endTime) / 30);
        $endTotal = substr_count(substr($str_48, 0, $endTimeNum), '1') * 30 / 60;
        $actual = (($endDate - $startDate) / 86400) - 1;
        $total = $totalTime * $actual + $startTotal + $endTotal;
        var_dump($total);
    }

}
