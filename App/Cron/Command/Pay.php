<?php


namespace App\Cron\Command;


use App\Common\HttpClients;
use EasySwoole\EasySwoole\Command\CommandInterface;

class Pay implements CommandInterface
{
    public function commandName(): string
    {
        return 'pay';
    }

    public function exec(array $args): ?string
    {
        $key = 'E607037D4CC34AA3B1C9A01B';
        $url = 'https://pay.heemoney.com/v1/ApplyPay';
        $data = [
            'method' => 'heemoney.pay.applypay',
            'version' => '1.0',
            'app_id' => 'hyp200108124376000022486059E146E',
            'mch_uid' => '1243762124445',
            'charset' => 'UTF-8',
            'timestamp' => date('YmdHis'),
            'sign_type' => 'MD5'
        ];
        $data['biz_content'] = json_encode([
            'out_trade_no' => 'HB' . time() . mt_rand(),
            'subject' => '测试',
            'total_fee' => 100,
            'channel_type' => 'ALI_WAP',
            'client_ip' => '192.168.88.88',
            'notify_url' => 'http://demos.shuzi88.com'
        ]);
        $data['sign'] = $this->sign($data, $key);
//        self::post($url,json_encode($data),false,["Content-type"=>"application/json;charset='utf-8'"]);
        go(function () use ($url, $data) {
//            dd( json_encode($data),'123');
//            exit();
            $result = HttpClients::getInstance()->http($url, json_encode($data), 'POST',[],["Content-type"=>"application/json;charset='utf-8'"]);
            var_dump(json_decode($result,true));
        });
        return 1;
        // TODO: Implement exec() method.
    }

    public function sign($params, $key)
    {
        ksort($params);
        $formatData = [];
        foreach ($params as $k => $v) {
            if (!empty($v) && $k != 'sign') {
                $formatData[] = "$k=$v";
            }
        }
        $signStr = implode('&', $formatData) . '&key=' . $key;
        return strtoupper(md5($signStr));
    }


    public function help(array $args): ?string
    {
        // TODO: Implement help() method.
    }

     public static function post($url, $param, $post_file = false, $header = [])
    {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        if (!empty($header)) {
            curl_setopt($oCurl, CURLOPT_HTTPHEADER, $header);
        }
        if (is_string($param) || $post_file) {
            $strPOST = $param;
        } else {
            $aPOST = array();
            foreach ($param as $key => $val) {
                $aPOST[] = $key . "=" . urlencode($val);
            }
            $strPOST = join("&", $aPOST);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_POST, true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $strPOST);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
//        dump(json_decode($sContent));
        if (intval($aStatus["http_code"]) == 200) {
            return $sContent;
        } else {
            return false;
        }
    }

}
