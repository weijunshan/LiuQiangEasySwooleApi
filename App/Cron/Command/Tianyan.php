<?php


namespace App\Cron\Command;


use App\Common\RedisSync;
use App\Models\UserModel;
use EasySwoole\EasySwoole\Command\CommandInterface;
use EasySwoole\FastCache\Cache;
use EasySwoole\RedisPool\Redis;
use Swoole\Coroutine;

class Tianyan implements CommandInterface
{
    public $url = 'https://top.tianyancha.com/companies';

    public $totalPage = 0;

    public $headers = [
        'Accept ' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding' => 'gzip, deflate, br',
        'Accept-Language' => 'zh-CN,zh;q=0.9,en;q=0.8',
        'Cache-Control' => 'no-cache',
        'Connection' => 'keep-alive',
        'Cookie' => 'jsid=SEM-BAIDU-PZ1907-SY-000100; TYCID=824ad660c3cf11e997c3e7d4c9a3e453; undefined=824ad660c3cf11e997c3e7d4c9a3e453; ssuid=2368589268; _ga=GA1.2.992828815.1566363155; _gid=GA1.2.983361358.1575854511; RTYCID=a42fd5bcc1f74eeda7c87819695eb565; CT_TYCID=fef619d420d14a1589fc75e974cb2561; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1573289381,1574748735,1575854511,1575877551; auth_token=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxODI4MjY2OTgzMiIsImlhdCI6MTU3MzI4OTYzMywiZXhwIjoxNjA0ODI1NjMzfQ.8otow_CY00i8jd_GOmzWfTMhgHs2NGil8FHFUrxyUrR5Gwfyi2-acdKZmdxWloKzBkN_qXTTZ_6M1SKBx7n0zg; tyc-user-info=%257B%2522claimEditPoint%2522%253A%25220%2522%252C%2522myAnswerCount%2522%253A%25220%2522%252C%2522myQuestionCount%2522%253A%25220%2522%252C%2522signUp%2522%253A%25220%2522%252C%2522explainPoint%2522%253A%25220%2522%252C%2522privateMessagePointWeb%2522%253A%25220%2522%252C%2522nickname%2522%253A%2522%25E9%25BB%2591%25E8%25B1%25B9%2522%252C%2522integrity%2522%253A%25220%2525%2522%252C%2522privateMessagePoint%2522%253A%25220%2522%252C%2522state%2522%253A0%252C%2522announcementPoint%2522%253A%25220%2522%252C%2522isClaim%2522%253A%25220%2522%252C%2522bidSubscribe%2522%253A%2522-1%2522%252C%2522vipManager%2522%253A%25220%2522%252C%2522discussCommendCount%2522%253A%25220%2522%252C%2522monitorUnreadCount%2522%253A%252275%2522%252C%2522onum%2522%253A%25220%2522%252C%2522claimPoint%2522%253A%25220%2522%252C%2522token%2522%253A%2522eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxODI4MjY2OTgzMiIsImlhdCI6MTU3MzI4OTYzMywiZXhwIjoxNjA0ODI1NjMzfQ.8otow_CY00i8jd_GOmzWfTMhgHs2NGil8FHFUrxyUrR5Gwfyi2-acdKZmdxWloKzBkN_qXTTZ_6M1SKBx7n0zg%2522%252C%2522pleaseAnswerCount%2522%253A%25220%2522%252C%2522redPoint%2522%253A%25220%2522%252C%2522bizCardUnread%2522%253A%25220%2522%252C%2522vnum%2522%253A%25220%2522%252C%2522mobile%2522%253A%252218282669832%2522%257D; aliyungf_tc=AQAAAIrvsQh6zw4AEzS23mkz2GbxITL2; csrfToken=zybHvN6KbU4IbMg0mtS5gd17; bannerFlag=undefined; Hm_lvt_ded2b36577e77da320c91b90209bf7ac=1575877952,1575944764,1575962141,1575962303; Hm_lpvt_ded2b36577e77da320c91b90209bf7ac=1575962303',
        'Host' => 'top.tianyancha.com',
        'Pragma' => 'no-cache',
        'Sec-Fetch-Mode' => 'navigate',
        'Sec-Fetch-Site' => 'none',
        'Sec-Fetch-User' => '?1',
        'Upgrade-Insecure-Requests' => 1,
        'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
    ];


    public function commandName(): string
    {
        return 'tianyan'; //命令行名字
    }

    //爬取天眼查数据
    public function exec(array $args): ?string
    {
        go(function () {
            $html = $this->http_req($this->url);
            $hrefList = $this->xpaht($html, "//div[@class='header']/a/@href");
            foreach ($hrefList as $v) {
                go(function () use ($v) {
                    $html = $this->http_req($v);
                    file_put_contents('123.html',$html);
                    $content = $this->xpaht($html,"//div[@class='header']");
                    var_dump($content);
                });
            }
        });
        return '执行中';
    }

    public function help(array $args): ?string
    {
        // TODO: Implement help() method.
    }


    public function test()
    {
        go(function () {
//            $this->init();
//            $res = RedisSync::getInstance()->get('tianyan');
//            $start = $res ? $res : 1;
//            foreach (range($start, $this->totalPage) as $v) {
//                $url = $this->url . '/p' . $v;
//                $html = $this->http_req($url);
//                $resUrlList = $this->analysisHtml($html, '/<a href="(.+)" class="name">.+<\/a>/');
//                $this->SecondaryAnalysis($resUrlList);
//                if (($v % 5) == 0) {
//                    RedisSync::getInstance()->set('tianyan', $v,300);
//                    break;
//                }
//            }

        });
    }


    /**
     * @throws \EasySwoole\HttpClient\Exception\InvalidUrl
     * 初始化获取总条数
     */
    public function init()
    {
        $html = $this->http_req($this->url);
//        $a = $this->xpaht($html, "//a[@class='num -next']/@href");
//        var_dump($a);
        /*        $page = $this->analysisHtml($html, '/<a class="num -end"[\s\S]*?>.*?(\d+)<\/a>/');*/
//        $totalPage = str_replace('...', '', $page[0]);
//        $this->totalPage = $page[0];
    }

    /**
     * @param $html
     * @param $preg_match string 正则匹配规则
     * @return array
     * xpath 匹配HTML数据
     */

    public function analysisHtml($html, $preg_match)
    {
        preg_match_all($preg_match, $html, $matches);
        return $matches[1];
    }

    //xpath 匹配有
    //todo xpaht 匹配了多次就无法匹配成功
    public function xpaht($html, $xpath)
    {
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true); //屏蔽loadHTML 警告错误
        $dom->loadHTML($html);
        $dom->normalize();
        $xpathObj = new \DOMXPath($dom);
        $hrefList = $xpathObj->query($xpath);
        $res = [];
        for ($i = 0; $i < $hrefList->length; $i++) {
            $href = $hrefList->item($i);
            array_push($res, $href->nodeValue);
        }
        return $res;
    }

    /**
     * @throws \EasySwoole\HttpClient\Exception\InvalidUrl
     */
    public function http_req($url, $data = [], $method = "GET", $cookie = [], $headers = [])
    {
        $httpClient = new \EasySwoole\HttpClient\HttpClient($url);
        if (!empty($cookie)) {
            foreach ($cookie as $k => $v) {
                $httpClient->addCookie($k, $v);
            }
        }
        if (!empty($this->headers)) {
            $httpClient->setHeaders($this->headers);
        };
        if ($method != 'GET') {
            $ret = $httpClient->post($data);
        } else {
            $ret = $httpClient->get($data);
        }
        return $ret->getBody();
    }

    /**
     * @param  $urlList
     * 二次分析链接
     */
    public function SecondaryAnalysis($urlList)
    {
        var_dump($urlList);
    }


}
