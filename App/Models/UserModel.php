<?php


namespace App\Models;


use EasySwoole\Component\Singleton;
use EasySwoole\Mysqli\QueryBuilder;

class UserModel extends BaseModel
{
    use Singleton;

    protected $tableName = 'system_user';

    protected $connectionName = 'liuqiang';

    public function userFindOne($username)
    {
        $res = $this->where(['username' => $username])->findOne();
        return $res;
    }

    public function list($page = 1, $limit = 10, $search = [])
    {
        $field = '*';
        $model = $this->where(function (QueryBuilder $query) use ($search) {
            foreach ($search as $key => $value) {
                if ($key == 'username' && !empty($value)) {
                    $query->where('username', $value);
                } else {
                    !empty($value) && $query->where($key, '%' . $value . '%', 'LIKE');
                }
            }
        })->field($field)->page($page, $limit)->withTotalCount();
        $data = $model->select();
        $count = $model->lastQueryResult()->getTotalCount();
        $list = $this->setPage($limit, $page, $count, $data);
        return $list;
    }


}
