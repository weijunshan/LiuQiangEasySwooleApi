<?php


namespace App\Models;


use EasySwoole\Component\Singleton;

class AuthModel extends BaseModel
{
    use Singleton;
    protected $tableName = 'system_auth';

    public function list($page, $limit, $search)
    {
        //todo 搜索
        $modelData = $this->field('id,title,status,sort,remark,create_at')->page($page, $limit)->withTotalCount();
        $data = $modelData->select();
        $count = $modelData->lastQueryResult()->getTotalCount();
        $list = $this->setPage($limit, $page, $count, $data);
        return $list;
    }
}
