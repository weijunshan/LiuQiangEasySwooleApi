<?php


namespace App\Models;


use EasySwoole\ORM\AbstractModel;

class BaseModel extends AbstractModel
{
    /**
     * @param $limit int 显示条数
     * @param $page int 页码
     * @param $count int 总条数
     * @param $data array 数据
     * @return array
     */
    protected function setPage($limit, $page, $count, $data)
    {
        empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit' => $limit,
            'page' => $page,
            'total_page' => ceil($count / $limit),
        ];
        $list = [
            'msg' => $msg,
            'total_number' => $count,
            'page_info' => $info,
            'data' => $data,
        ];
        return $list;
    }
}
