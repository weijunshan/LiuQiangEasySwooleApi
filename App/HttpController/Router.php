<?php


namespace App\HttpController;


use EasySwoole\Http\AbstractInterface\AbstractRouter;
use FastRoute\RouteCollector;
use Swoole\Http\Request;
use Swoole\Http\Response;

class Router extends AbstractRouter
{
    function initialize(RouteCollector $routeCollector)
    {
        $this->setGlobalMode(true); //设置全局路由
        // 登录
        $routeCollector->addRoute(['GET', 'POST'], '/user/login', '/api/system/user/login');
        //首页
        $routeCollector->addRoute(['GET', 'POST'], '/index/index', '/api/index/index/index');
        $routeCollector->addRoute(['GET', 'POST'], '/', '/api/index/index/index');
        /**** 菜单列表***/
        $routeCollector->addRoute(['GET', 'POST'], '/menu/index', '/api/system/menu/index');//后台菜单列表
        $routeCollector->addRoute(['GET', 'POST'], '/menu/add', '/api/system/menu/add');// 增加菜单
        $routeCollector->addRoute(['GET', 'POST'], '/menu/edit', '/api/system/menu/edit');//修改
        $routeCollector->addRoute(['GET', 'POST'], '/menu/status', '/api/system/menu/status');//状态
        $routeCollector->addRoute(['GET', 'POST'], '/menu/del', '/api/system/menu/del');//删除
        /*** 节点管理 ***/
        $routeCollector->addRoute(['GET', 'POST'], '/node/index', '/api/system/node/index');//获取节点列表
        $routeCollector->addRoute(['GET', 'POST'], '/node/refresh', '/api/system/node/refresh');//刷新节点
        $routeCollector->addRoute(['GET', 'POST'], '/node/edit_is_auth', '/api/system/node/edit_is_auth');//修改是否加入rabc权限
        $routeCollector->addRoute(['GET', 'POST'], '/node/name', '/api/system/node/name');//节点名称
        /*** 后台用户管理 ***/
        $routeCollector->addRoute(['GET', 'POST'], '/user/list', '/api/system/user/list');//获取用户列表
        $routeCollector->addRoute(['GET', 'POST'], '/user/add', '/api/system/user/add');//添加用户
        $routeCollector->addRoute(['GET', 'POST'], '/user/edit', '/api/system/user/edit');//编辑用户信息
        $routeCollector->addRoute(['GET', 'POST'], '/user/info', '/api/system/user/info');//获取用户信息
        $routeCollector->addRoute(['GET', 'POST'], '/user/del', '/api/system/user/del');//删除用户
        $routeCollector->addRoute(['GET', 'POST'], '/user/status', '/api/system/user/status');//get post 拦截 用户状态
        $routeCollector->addRoute(['GET', 'POST'], '/user/edit_password', '/api/system/user/edit_password');//用户状态 修改
        /*** 后台角色权限管理***/
        $routeCollector->addRoute(['GET', 'POST'], '/auth/list', '/api/system/auth/list');//获取用户列表
        $routeCollector->addRoute(['GET', 'POST'], '/auth/del', '/api/system/auth/del');//删除角色
        $routeCollector->addRoute(['GET', 'POST'], '/auth/status', '/api/system/auth/status');//修改状态
        $routeCollector->addRoute(['GET', 'POST'], '/auth/add', '/api/system/auth/add');//增加角色
        $routeCollector->addRoute(['GET', 'POST'], '/auth/edit', '/api/system/auth/edit');//修改角色
        $routeCollector->addRoute(['GET', 'POST'], '/auth/apply', '/api/system/auth/apply');//修改角色

    }


}
