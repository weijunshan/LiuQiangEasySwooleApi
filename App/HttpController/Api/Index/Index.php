<?php


namespace App\HttpController\Api\Index;


use App\HttpController\Api\Base;
use EasySwoole\Validate\Validate;

class Index extends Base
{
    public function index()
    {
        $this->writeJson(['todo' . '首页数据']);
        return false;
    }

    protected function getValidateRule(?string $action): ?Validate
    {
        // TODO: Implement getValidateRule() method.
    }
}
