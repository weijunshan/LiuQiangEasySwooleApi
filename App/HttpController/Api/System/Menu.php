<?php


namespace App\HttpController\Api\System;


use App\HttpController\Api\Base;
use App\Models\MenuModel;
use EasySwoole\Http\Message\Status;
use EasySwoole\Validate\Validate;

//菜单栏目
class Menu extends Base
{
    /**
     * 菜单栏目列表显示
     */
    public function index()
    {
        $MenuModel = new MenuModel();
        $menu = $MenuModel->getAllMenu();
        $this->writeJson($menu);
    }

    public function add()
    {
        $addData = $this->request()->getParsedBody();
        try {
            $menuModel = MenuModel::create([
                'pid' => $addData['pid'],
                'title' => $addData['title'],
                'icon' => $addData['icon'],
                'href' => $addData['href'],
                'params' => $addData['params'],
                'sort' => empty($addData['sort']) ? 0 : $addData['sort'],
            ]);
            $menuModel->save();
            $this->writeJson($this->success('添加栏目成功'));
        } catch (\Throwable $t) {
            $this->writeJson($this->error('添加栏目异常'));
            $this->errorLog($t);
        }
        return false;
    }


    public function edit()
    {
        $addData = $this->request()->getParsedBody();
        try {
            MenuModel::create()->update([
                'pid' => $addData['pid'],
                'title' => $addData['title'],
                'icon' => $addData['icon'],
                'href' => $addData['href'],
                'params' => $addData['params'],
                'sort' => empty($addData['sort']) ? 0 : $addData['sort'],
            ], ['id' => $addData['id']]);
            $this->writeJson($this->success('修改栏目成功'));
        } catch (\Throwable $t) {
            $this->writeJson($this->error('修改栏目异常'));
            $this->errorLog($t);
        }
        return false;
    }

    public function status()
    {
        $addData = $this->request()->getParsedBody();
        $status = $addData['status'] ? '启用' : '禁用';
        try {
            MenuModel::create()->update([
                'status' => $addData['status']
            ], ['id' => $addData['id']]);
            $this->writeJson($this->success($status . '栏目成功'));
        } catch (\Throwable $t) {
            $this->writeJson($this->error($status . '栏目异常'));
            $this->errorLog($t);
        }
        return false;
    }

    public function del()
    {
        $addData = $this->request()->getParsedBody();
        try {
            MenuModel::create()->destroy(['id' => $addData['id']]);
            $this->writeJson($this->success('删除栏目成功'));
        } catch (\Throwable $t) {
            $this->writeJson($this->error('删除栏目异常'));
            $this->errorLog($t);
        }
        return false;
    }


    protected function getValidateRule(?string $action): ?Validate
    {
        $validate = new Validate();
        switch ($action) {
            case 'add':
                $validate->addColumn('pid', '父id')->required()->numeric()->min(0);
                $validate->addColumn('title', '栏目名称')->required();
                $validate->addColumn('href', '链接必须填写')->required();
                break;
            case 'edit':
                $validate->addColumn('pid', '父id')->required()->numeric()->min(0);
                $validate->addColumn('title', '栏目名称')->required();
                $validate->addColumn('href', '链接必须填写')->required();
                $validate->addColumn('id', '菜单id')->required();
                break;
            case 'status':
                $validate->addColumn('id', '菜单id')->required();
                $validate->addColumn('status', '菜单状态')->required()->min(0)->max(1);
                break;
            case 'del':
                $validate->addColumn('id', '菜单id')->required();
                break;
        }
        return $validate;
    }


}
