<?php


namespace App\Common;


use EasySwoole\Component\Singleton;
use EasySwoole\Http\Message\Status;

class JsonSendMessage
{
    use Singleton;

    /**
     * @param null $result
     * @param string $msg
     * @param int $statusCode
     * @return false|string
     * 成功消息格式
     */
    public function successJson($result = NULL, $msg = 'OK', $statusCode = Status::CODE_OK)
    {
        $data = [
            "code" => $statusCode, //状态码
            "result" => $result,  //结果
            "msg" => $msg  //提示
        ];
        return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param string $msg
     * @param int $statusCode
     * @return false|string
     *  失败消息处理
     */
    public function errorJson($msg = 'fail', $statusCode = Status::CODE_BAD_REQUEST)
    {
        $data = [
            "code" => $statusCode, //状态码
            "result" => new \stdClass(),  //结果
            "msg" => $msg  //提示
        ];
        return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

    }
}
