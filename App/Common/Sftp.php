<?php


namespace App\Common;


class Sftp
{
    private $config = [
        'host' => '127.0.0.1', //服务器
        'port' => '22', //端口
        'username' => 'root', //用户名
        'password' => '+liu+qiang+1002', //密码
    ];
    private $conn = NULL;

    /**
     * Sftp constructor.
     * @param $config
     * @throws \Exception
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->connect();
    }

    /**
     * @throws \Exception
     * 连接 sftp服务
     */
    public function connect()
    {
        $this->conn = ssh2_connect($this->config['host'], $this->config['port']);
        if (!ssh2_auth_password($this->conn, $this->config['username'], $this->config['password'])) {
            echo '无法在服务器进行身份验证';
            exit();
        }

    }

    /**
     * @param $remote string 远程地址
     * @param $local string 保存本地路径
     * @return bool  下载结果
     * 传输数据 传输层协议,获得数据
     * 下载
     */
    public function download($remote, $local)
    {
        $ressftp = ssh2_sftp($this->conn);
        return copy("ssh2.sftp://{$ressftp}" . $remote, $local);
    }

    /**
     * @param $local  string 本地路径
     * @param $remote string 远程路径
     * @param int $file_mode 设置权限
     * @return bool 结果
     * 传输数据 传输层协议,写入ftp服务器数据
     * 上传
     */
    public function upload($local, $remote, $file_mode = 0777)
    {
        $ressftp = ssh2_sftp($this->conn);
        return copy($local, "ssh2.sftp://{$ressftp}" . $remote);
    }
}
