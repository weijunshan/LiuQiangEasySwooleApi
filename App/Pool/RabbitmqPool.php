<?php


namespace App\Pool;


use EasySwoole\Pool\AbstractPool;
use EasySwoole\Pool\Config;
use PhpAmqpLib\Connection\AMQPStreamConnection;

// Rabbitmq 三方中间介
class RabbitmqPool extends AbstractPool
{
    protected $rabbitmqConfig;

    public function __construct(Config $conf, $RabbitmqConfig)
    {
        parent::__construct($conf);
        $this->rabbitmqConfig = $RabbitmqConfig;
    }

    protected function createObject()
    {
        $conf = $this->rabbitmqConfig;
        $rabb = new AMQPStreamConnection($conf['host'], $conf['port'], $conf['user'], $conf['pwd'], $conf['vhost']);
        return $rabb->channel();
    }

}
