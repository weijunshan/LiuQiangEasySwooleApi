<?php


namespace App\Pool;


use EasySwoole\Pool\AbstractPool;
use EasySwoole\Pool\Config;
use EasySwoole\Redis\Config\RedisConfig;
use EasySwoole\Redis\Redis;

class RedisPool extends AbstractPool
{
    protected $redisConf;

    public function __construct(Config $conf, RedisConfig $redisConfig)
    {
        parent::__construct($conf);
        $this->redisConf = $redisConfig;
    }

    //创建redis对象
    protected function createObject()
    {
        $redis = new Redis($this->redisConf);
        return $redis;
    }
}
