<?php


namespace App\Socket\Tcp;


use App\Common\JsonSendMessage;
use App\Socket\WebSocket\MessageServer\BaseMessage;
use EasySwoole\Component\Timer;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\Socket\Client\WebSocket;

class TcpMessage extends BaseMessage
{

    public function __construct()
    {
        parent::__construct();

    }

    public function run()
    {
        $content = $this->caller()->getArgs();
        if (isset($content['name'])) {
            $this->response()->setMessage(JsonSendMessage::getInstance()->successJson(
                [
                    'msg' => '你好' . $content['name'],
                    'time' => date('Y-m-d H:i:s')
                ]));
        }

        Timer::getInstance()->loop(2000,function (){
            $client = $this->caller()->getClient();
            $server = ServerManager::getInstance()->getSwooleServer();
            $server->push($client->getFd(),JsonSendMessage::getInstance()->successJson(
                [
                    'msg' => '我是定时消息推送',
                    'time' => date('Y-m-d H:i:s')
                ]));
        });

    }
}
