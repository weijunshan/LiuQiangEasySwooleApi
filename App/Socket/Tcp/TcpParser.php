<?php


namespace App\Socket\Tcp;


use EasySwoole\Socket\AbstractInterface\ParserInterface;
use EasySwoole\Socket\Bean\Caller;
use EasySwoole\Socket\Bean\Response;

class TcpParser implements ParserInterface
{

    public function decode($raw, $client): ?Caller
    {
        $data = substr($raw, '4');
        // 解析 客户端接受原始消息
        $data = json_decode($data, true);
//        if (!is_array($data)) {
//            $this->sendMessage($client, $this->errorJson('消息格式错误'));
//            return null;
//        }
        // new 调用者原始对象
        $caller = new Caller();
        // 设置传达处理消息业务逻辑的类  默认消息处理类WebSocketMessage
        $class = '\\App\\Socket\\Tcp\\' . ucfirst($data['class'] ?? 'TcpMessage');

        $caller->setControllerClass($class);
        // 设置被调用的方法  默认方法 run
        $caller->setAction($data['action'] ?? 'run');
        // 检查是否存在 content 消息内容处理
        if (!empty($data['content'])) {
            // content 无法解析为array 时 返回 content => string 格式
            $args = is_array($data['content']) ? $data['content'] : ['content' => $data['content']];
        }
        // 设置被调用的Args
        $caller->setArgs($args ?? []);
        return $caller;
    }

    public function encode(Response $response, $client): ?string
    {
        return pack('N', strlen($response->getMessage())) . $response->getMessage();
    }
}
