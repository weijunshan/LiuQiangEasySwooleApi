<?php


namespace App\Socket\WebSocket\MessageServer;


use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\Http\Message\Status;
use EasySwoole\Socket\AbstractInterface\Controller;
use EasySwoole\Socket\Client\WebSocket;


abstract class BaseMessage extends Controller
{

    /**
     * @param WebSocket $client WebSocket Client 对象
     * @param $data string 推送的消息
     */
    protected function sendMessage($client, $data)
    {
        go(function () use ($client, $data) {
            $server = ServerManager::getInstance()->getSwooleServer();
            if ($server->isEstablished($client->getFd())) {
                $server->push($client->getFd(), $data);
            }
        });
    }


    // 全部有效连接消息推送
    protected function pushAll($msg)
    {
        $server = ServerManager::getInstance()->getSwooleServer();
        $conn_list = $server->getClientList(0);
        if ($conn_list) {
            foreach ($conn_list as $fd) {
                if ($server->isEstablished($fd)) {
                    $server->push($fd, $this->successJson($msg));
                }
            }
        }
    }

    abstract public function run();
}
