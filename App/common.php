<?php
// 公共文件
function dd($data, $file = 'debug')
{
    go(function () use ($data, $file) {
        $path = dirname(__DIR__) . '/debug/' . $file . '.txt';
        file_put_contents($path, print_r($data, true) . PHP_EOL, FILE_APPEND);
    });
}
