/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.88.88
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : 127.0.0.1:3306
 Source Schema         : easy_api

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 27/11/2019 18:04:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_auth
-- ----------------------------
DROP TABLE IF EXISTS `system_auth`;
CREATE TABLE `system_auth`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `sort` smallint(6) UNSIGNED NULL DEFAULT 0 COMMENT '排序权重',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_by` bigint(11) UNSIGNED NULL DEFAULT 0 COMMENT '创建人',
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_at` timestamp(0) NULL DEFAULT NULL,
  `update_by` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_system_auth_title`(`title`) USING BTREE,
  INDEX `index_system_auth_status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_auth
-- ----------------------------
INSERT INTO `system_auth` VALUES (0, '超级管理员', 1, 1, '不受权限控制', 0, '2018-01-23 13:28:14', NULL, NULL);
INSERT INTO `system_auth` VALUES (1, '管理员', 1, 4, '测试管理员', 0, '2018-03-17 15:59:46', '2018-08-07 10:26:57', NULL);
INSERT INTO `system_auth` VALUES (6, '测试权限', 1, 0, '4242543', 0, '2018-09-22 18:15:31', NULL, NULL);

-- ----------------------------
-- Table structure for system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `system_auth_node`;
CREATE TABLE `system_auth_node`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '角色ID',
  `node` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点路径',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_system_auth_auth`(`auth`) USING BTREE,
  INDEX `index_system_auth_node`(`node`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 174 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与节点关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_auth_node
-- ----------------------------
INSERT INTO `system_auth_node` VALUES (122, 1, '1665');
INSERT INTO `system_auth_node` VALUES (123, 1, '1663');
INSERT INTO `system_auth_node` VALUES (124, 1, '1664');
INSERT INTO `system_auth_node` VALUES (125, 1, '1665');
INSERT INTO `system_auth_node` VALUES (126, 1, '1666');
INSERT INTO `system_auth_node` VALUES (127, 1, '1667');
INSERT INTO `system_auth_node` VALUES (128, 1, '1668');
INSERT INTO `system_auth_node` VALUES (129, 1, '1669');

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量值',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注信息',
  `sort` int(10) NULL DEFAULT 0,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` bigint(20) NULL DEFAULT 0 COMMENT '创建人',
  `update_at` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES (1, 'ManageName', 'basic', 'string', '支付管理系统', '后台名称', 0, '2018-07-17 17:27:27', 0, '2018-07-17 22:10:33', NULL);
INSERT INTO `system_config` VALUES (2, 'Beian', 'basic', 'string', '蜀ICP备19010392号', '备案号', 4, '2018-07-17 17:27:27', 0, '2018-07-17 22:10:39', NULL);
INSERT INTO `system_config` VALUES (18, 'FooterName', 'basic', 'string', 'Copyright © 2018-2019 千亿科技', '底部网站标识', 5, '2018-07-17 17:27:27', 0, '2018-07-17 18:40:16', NULL);
INSERT INTO `system_config` VALUES (19, 'BeianUrl', 'basic', 'string', 'http://www.miitbeian.gov.cn', '备案查询链接', 2, '2018-07-17 17:30:39', 0, '2018-07-17 17:31:22', NULL);
INSERT INTO `system_config` VALUES (20, 'HomeUrl', 'basic', 'string', 'https://www.shuzi.com', '网站首页', 0, '2018-07-17 18:45:59', 0, '2018-07-17 18:46:12', NULL);
INSERT INTO `system_config` VALUES (21, 'VercodeType', 'basic', 'tinyint', '1', '验证码登录开关（0：不开启，1：开启）', 3, '2018-07-17 21:52:00', 0, '2018-07-18 02:38:10', NULL);
INSERT INTO `system_config` VALUES (32, 'Describe', 'basic', 'string', 'RBAC后台权限控制系统', '网站描述', 9, '2018-07-30 23:01:34', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (33, 'Author', 'basic', 'string', 'My.Liu', '作者', 15, '2018-07-30 23:02:41', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (34, 'Email', 'basic', 'string', '872871448@qq.com', '联系邮箱', 8, '2018-07-30 23:03:15', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (35, 'BlogFooterName', 'basic', 'string', 'Copyright © 2018-2019 千亿科技', '博客底部', 0, '2018-08-13 00:32:50', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (36, 'MailHost', 'mail', 'string', 'smtp.163.com', '发送方的SMTP服务器地址', 0, '2018-08-31 15:39:04', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (37, 'MailUsername', 'mail', 'string', '', '发送方的QQ邮箱用户名', 0, '2018-08-31 15:39:43', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (38, 'MailPassword', 'mail', 'string', '', '第三方授权登录码', 0, '2018-08-31 15:39:53', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (39, 'MailNickname', 'mail', 'string', '成都千亿科技有限公司', '设置发件人昵称', 0, '2018-08-31 15:40:44', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (40, 'MailReplyTo', 'mail', 'string', '18282669832@163.com', '回复邮件地址', 0, '2018-08-31 15:41:03', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (41, 'AccessKeyId', 'sms', 'string', '', '阿里大于公钥', 0, '2018-08-31 23:58:34', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (42, 'AccessKeySecret', 'sms', 'string', '', '阿里大鱼私钥', 0, '2018-08-31 23:58:45', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (43, 'SignName', 'sms', 'string', '无', '短信注册模板', 0, '2018-09-01 00:08:55', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (44, 'CodeTime', 'code', 'int', '60', '验证码发送间隔时间', 0, '2018-09-04 18:03:52', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (45, 'CodeDieTime', 'code', 'int', '300', '验证码有效期', 0, '2018-09-04 18:17:26', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (46, 'FileType', 'file', 'int', '1', '文件保存方法（1：本地，2：七牛云）', 0, '2018-09-17 11:44:12', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (47, 'FileKey', 'file', 'string', '690c7175d2b4439646b437b8b48f92fb147eccf0', '文件路径加密秘钥（www.99php.cn）', 0, '2018-09-17 16:51:29', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (48, 'LoginDuration', 'basic', 'int', '7200', '后台登录有效时间', 0, '2018-09-30 01:02:53', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (49, 'AdminModuleName', 'basic', 'int', 'admin', '后台登录模块名', 0, '2018-10-01 01:22:05', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (50, 'CleanCachePassword', 'basic', 'string', 'chung951222', '刷新缓存的密码', 0, '2018-10-01 01:42:16', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (51, 'spider_access_key', 'spider', 'string', 'asdfmigshjogsn', '采集接口公钥', 0, '2018-11-19 10:46:26', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (52, 'spider_secret_key', 'spider', 'string', 'twjtrowmlca', '采集接口私钥', 0, '2018-11-19 10:46:36', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (53, 'spider_url', 'spider', 'string', 'http://spider.99php.cn/api/article/index.html', '采集接口地址', 0, '2018-11-19 10:46:46', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (54, 'HuiYuan_receipt_account', 'sys', 'int', '1384', '汇元自动收款微信ID', 0, '2019-07-23 18:03:36', 0, NULL, NULL);
INSERT INTO `system_config` VALUES (55, 'VercodeSms', 'basic', 'tinyint', '1', '后台登录短信验证开关（0：不开启，1：开启）', 0, '2019-08-09 10:52:23', 0, NULL, NULL);

-- ----------------------------
-- Table structure for system_log
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '商户id',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `action` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 331084 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_log
-- ----------------------------
INSERT INTO `system_log` VALUES (331053, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.05秒,内存消耗:4.32M', '2019-07-25 00:30:01');
INSERT INTO `system_log` VALUES (331054, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-25 01:00:01');
INSERT INTO `system_log` VALUES (331055, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.05秒,内存消耗:4.47M', '2019-07-26 00:30:01');
INSERT INTO `system_log` VALUES (331056, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-26 01:00:01');
INSERT INTO `system_log` VALUES (331057, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.05秒,内存消耗:4.57M', '2019-07-27 00:30:01');
INSERT INTO `system_log` VALUES (331058, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-27 01:00:01');
INSERT INTO `system_log` VALUES (331059, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.05秒,内存消耗:4.55M', '2019-07-28 00:30:01');
INSERT INTO `system_log` VALUES (331060, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-28 01:00:01');
INSERT INTO `system_log` VALUES (331061, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:4.58M', '2019-07-29 00:30:01');
INSERT INTO `system_log` VALUES (331062, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-29 01:00:01');
INSERT INTO `system_log` VALUES (331063, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:4.59M', '2019-07-30 00:30:01');
INSERT INTO `system_log` VALUES (331064, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-30 01:00:02');
INSERT INTO `system_log` VALUES (331065, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:4.53M', '2019-07-31 00:30:02');
INSERT INTO `system_log` VALUES (331066, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-07-31 01:00:01');
INSERT INTO `system_log` VALUES (331067, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:4.57M', '2019-08-01 00:30:01');
INSERT INTO `system_log` VALUES (331068, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-01 01:00:01');
INSERT INTO `system_log` VALUES (331069, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:4.91M', '2019-08-02 00:30:01');
INSERT INTO `system_log` VALUES (331070, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-02 01:00:01');
INSERT INTO `system_log` VALUES (331071, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:5.16M', '2019-08-03 00:30:01');
INSERT INTO `system_log` VALUES (331072, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-03 01:00:01');
INSERT INTO `system_log` VALUES (331073, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:5.17M', '2019-08-04 00:30:01');
INSERT INTO `system_log` VALUES (331074, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-04 01:00:01');
INSERT INTO `system_log` VALUES (331075, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:5.06M', '2019-08-05 00:30:01');
INSERT INTO `system_log` VALUES (331076, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-05 01:00:01');
INSERT INTO `system_log` VALUES (331077, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:4.98M', '2019-08-06 00:30:01');
INSERT INTO `system_log` VALUES (331078, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-06 01:00:01');
INSERT INTO `system_log` VALUES (331079, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.08秒,内存消耗:5.01M', '2019-08-07 00:30:01');
INSERT INTO `system_log` VALUES (331080, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-07 01:00:01');
INSERT INTO `system_log` VALUES (331081, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.06秒,内存消耗:5.35M', '2019-08-08 00:30:01');
INSERT INTO `system_log` VALUES (331082, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-08 01:00:02');
INSERT INTO `system_log` VALUES (331083, 0, '127.0.0.1', 'php think count_order_data', 'System', 'Task', '[ 统计订单支付数据到汇总表每天凌晨 00:30执行] 共计:1商户,成功:1,失败:0,耗时:0.08秒,内存消耗:5.31M', '2019-08-09 00:30:01');
INSERT INTO `system_log` VALUES (331084, 0, '127.0.0.1', 'php think count_user_money', 'System', 'Task', '[添加钱到商户钱包 每天凌晨 1:00 执行] 共计:2商户,成功:2,失败:0,耗时:0.01秒,内存消耗:3.67M', '2019-08-09 01:00:01');

-- ----------------------------
-- Table structure for system_login_record
-- ----------------------------
DROP TABLE IF EXISTS `system_login_record`;
CREATE TABLE `system_login_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT 1 COMMENT '登录类型（0：退出，1：登录）',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '系统用户ID（0：账户不存在） 后台登陆id',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录IP地址',
  `country` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '国家',
  `region` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `isp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网络类型',
  `location` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态（0：失败，1：成功）',
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 743 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_login_record
-- ----------------------------

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `spread` tinyint(1) NULL DEFAULT 0,
  `node` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` float(11, 2) NULL DEFAULT 0.00 COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_by` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人',
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_at` timestamp(0) NULL DEFAULT NULL,
  `update_by` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_system_menu_node`(`node`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES (2, 0, '首页', 0, '', '', '#', '', '_self', 0.00, 1, NULL, 0, '2019-11-05 14:56:10', NULL, NULL);
INSERT INTO `system_menu` VALUES (3, 2, '首页', 0, '', '', '/index/index', '', '_self', 1.00, 1, NULL, 0, '2019-11-05 15:42:38', NULL, NULL);
INSERT INTO `system_menu` VALUES (4, 0, '系统设置', 0, '', '', '#', '', '_self', 1.00, 1, NULL, 0, '2019-11-16 09:41:07', NULL, NULL);
INSERT INTO `system_menu` VALUES (5, 4, '系统管理', 0, '', '', '#', '', '_self', 1.00, 1, NULL, 0, '2019-11-16 09:45:57', NULL, NULL);
INSERT INTO `system_menu` VALUES (6, 4, '菜单列表', 0, '', '', '/menu/index', '', '_self', 1.00, 1, NULL, 0, '2019-11-16 09:46:56', NULL, NULL);

-- ----------------------------
-- Table structure for system_nav
-- ----------------------------
DROP TABLE IF EXISTS `system_nav`;
CREATE TABLE `system_nav`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `href` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `sort` float(11, 2) NULL DEFAULT 0.00 COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_by` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人',
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_at` timestamp(0) NULL DEFAULT NULL,
  `update_by` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 226 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统快捷导航' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_nav
-- ----------------------------
INSERT INTO `system_nav` VALUES (214, '配置管理', '&#xe716;', 'admin/blog.config/index', 0.00, 1, '', 0, '2018-10-11 22:19:02', NULL, NULL);
INSERT INTO `system_nav` VALUES (215, '友情链接', '&#xe64d;', 'admin/blog.website_link/index', 0.00, 1, '', 0, '2018-10-11 22:19:32', NULL, NULL);
INSERT INTO `system_nav` VALUES (216, '公告管理', '&#xe667;', 'admin/blog.notice/index', 0.00, 1, '', 0, '2018-10-11 22:21:02', NULL, NULL);
INSERT INTO `system_nav` VALUES (218, '搜索排行', '&#xe649;', 'admin/blog.search_record/index', 0.00, 1, '', 0, '2018-10-11 22:23:32', NULL, NULL);
INSERT INTO `system_nav` VALUES (219, '搜索记录', '&#xe66e;', 'admin/blog.search/index', 0.00, 1, '', 0, '2018-10-11 22:23:54', NULL, NULL);
INSERT INTO `system_nav` VALUES (225, '图标管理-layui', 'fa-circle-o-notch', 'admin/tool.icon/index', 0.00, 1, '', 0, '2018-10-13 21:42:25', NULL, NULL);
INSERT INTO `system_nav` VALUES (226, '图标管理-fa', 'fa-crosshairs', 'admin/tool.icon/fa', 0.00, 1, '', 0, '2018-10-13 21:43:02', NULL, NULL);

-- ----------------------------
-- Table structure for system_node
-- ----------------------------
DROP TABLE IF EXISTS `system_node`;
CREATE TABLE `system_node`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `node` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点代码',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点标题',
  `type` tinyint(1) NULL DEFAULT 3 COMMENT '节点类型（1：模块，2：控制器，3：节点）',
  `is_auth` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动RBAC权限控制',
  `is_auto` tinyint(1) NULL DEFAULT 0 COMMENT '是否为系统自动刷新（0：是，1：手动添加）',
  `create_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` bigint(20) NULL DEFAULT NULL,
  `update_at` timestamp(0) NULL DEFAULT NULL,
  `update_by` bigint(20) NULL DEFAULT NULL,
  `route_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由地址',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_system_node_node`(`node`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1682 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_node
-- ----------------------------
INSERT INTO `system_node` VALUES (1640, '/api/system', '系统设置', 1, 1, 0, '2019-11-04 11:38:16', NULL, NULL, NULL, '0');
INSERT INTO `system_node` VALUES (1663, '/api/system/user', NULL, 2, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '0');
INSERT INTO `system_node` VALUES (1664, '/api/system/user/login', NULL, 3, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '/user/login');
INSERT INTO `system_node` VALUES (1665, '/api/index', NULL, 1, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '0');
INSERT INTO `system_node` VALUES (1666, '/api/index/index', NULL, 2, 0, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '0');
INSERT INTO `system_node` VALUES (1667, '/api/index/index/index', NULL, 3, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '/index/index');
INSERT INTO `system_node` VALUES (1668, '/api/system/menu', NULL, 2, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '0');
INSERT INTO `system_node` VALUES (1669, '/api/system/menu/index', NULL, 3, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '/menu/index');
INSERT INTO `system_node` VALUES (1670, '/api/system/menu/add', NULL, 3, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '/menu/add');
INSERT INTO `system_node` VALUES (1671, '/api/system/node', NULL, 2, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '0');
INSERT INTO `system_node` VALUES (1672, '/api/system/node/refresh', NULL, 3, 1, 0, '2019-11-04 12:33:37', NULL, NULL, NULL, '/node/refresh');
INSERT INTO `system_node` VALUES (1674, '/api/system/menu/edit', NULL, 3, 1, 0, '2019-11-15 09:53:50', NULL, NULL, NULL, '/menu/edit');
INSERT INTO `system_node` VALUES (1675, '/api/system/menu/status', NULL, 3, 1, 0, '2019-11-15 09:53:50', NULL, NULL, NULL, '/menu/status');
INSERT INTO `system_node` VALUES (1676, '/api/system/menu/del', NULL, 3, 1, 0, '2019-11-15 09:53:50', NULL, NULL, NULL, '/menu/del');
INSERT INTO `system_node` VALUES (1677, '/api/system/node/index', NULL, 3, 1, 0, '2019-11-15 09:53:50', NULL, NULL, NULL, '/node/index');
INSERT INTO `system_node` VALUES (1678, '/api/system/node/edit_is_auth', NULL, 3, 1, 0, '2019-11-19 15:16:54', NULL, NULL, NULL, '/node/edit_is_auth');
INSERT INTO `system_node` VALUES (1679, '/api/system/user/list', NULL, 3, 1, 0, '2019-11-19 15:16:54', NULL, NULL, NULL, '/user/list');
INSERT INTO `system_node` VALUES (1680, '/api/system/user/del', NULL, 3, 1, 0, '2019-11-19 15:16:54', NULL, NULL, NULL, '/user/del');
INSERT INTO `system_node` VALUES (1681, '/api/system/user/status', NULL, 3, 1, 0, '2019-11-19 15:16:54', NULL, NULL, NULL, '/user/status');
INSERT INTO `system_node` VALUES (1682, '/api/system/user/edit_password', NULL, 3, 1, 0, '2019-11-19 15:16:54', NULL, NULL, NULL, '/user/edit_password');

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限ID',
  `head_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/static/image/admin/face1.jpg' COMMENT '头像',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `qq` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系QQ',
  `mail` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系邮箱',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系手机号',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `login_at` datetime(0) NULL DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用,)',
  `is_deleted` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除状态(1:删除,0:未删)',
  `create_by` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '创建人',
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `update_at` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_system_user_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES (1, '[]', '/static/image/admin/face1.jpg', 'root', 'dbb1c112a931eeb16299d9de1f30161d', NULL, NULL, '18282669832', '', 0, NULL, 1, 0, 0, '2019-03-04 11:03:20', NULL, NULL);
INSERT INTO `system_user` VALUES (2, '1,3,5', '', 'qyfadmin', 'e10adc3949ba59abbe56e057f20f883e', '872871448', '872871448@qq.com', '18282669832', '', 0, NULL, 0, 1, 0, '2019-04-30 15:26:50', NULL, NULL);
INSERT INTO `system_user` VALUES (6, '', '', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '872871448', '872871448@qq.com', '18282669832', '', 0, NULL, 1, 0, 0, '2019-11-27 14:42:38', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
